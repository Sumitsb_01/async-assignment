/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs')
const problem1 = (callback) => {

    fs.mkdir('randomfolder',(err) => {

        if (err){

            callback(err,null)

        }else{

            callback(null,'Folder created successfully\n')

            for(let i=1;i<11;i++){

                let filedata = {"employee": { "name":"sonoo","salary":56000,"married":true},'filenumber':i}
                let filename = `A${i}.json`
                fs.writeFile(`./randomfolder/${filename}`,JSON.stringify(filedata),(err) => {

                    if (err){

                        callback(err,null)
                    
                    }else {

                        callback(null,`File ${filename} created\n`)
                        fs.unlink(`./randomfolder/${filename}`,(err) => {

                            if (err){

                                callback(err,null)

                            }else{

                                callback(null,`File ${filename} deleted\n`)

                            }
                        })
                    }
                })

            }

        }
    })
}

module.exports=problem1
