// /*1. Read the given file lipsum.txt
// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
// */

const fs = require('fs')

const problem2 = (callback) => {

    //Read the given file lipsum.txt
    fs.readFile('lipsum.txt', 'utf-8', (err, data) => {

        if (err) {

            callback(err, null)

        } else {

            callback(null, 'Read the data from lipsum.txt file')
            // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
            let fileName = 'new1.txt'
            fs.writeFile(fileName, JSON.stringify(data.toUpperCase()), (err) => {

                if (err) {

                    callback(err)

                } else {

                    callback(null, 'Write the data on new1.txt file')
                    fs.writeFile('filenames.txt', `${fileName}\n`, (err) => {

                        if (err) {
                            callback(err)
                        } else {

                            callback(null, `Write ${fileName} in filenames.txt`)
                            // 3. Read the new file and convert it to lower case. Then split the contents into sentences. 
                            //Then write it to a new file. Store the name of the new file in filenames.txt
                            fs.readFile(fileName, 'utf-8', (err, data1) => {

                                if (err) {

                                    callback(err)
                                }else {

                                    callback(null,'Read the file new1.txt')
                                    fileName = 'new2.txt'
                                    fs.writeFile(fileName,JSON.stringify(data1.toLowerCase().replaceAll('\n','').split('.')),(err) => {

                                        if(err){

                                            callback(err)

                                        }else{

                                            callback(null,'Write the data in new2.txt')
                                            fs.appendFile('filenames.txt',`${fileName}\n`,(err) => {

                                                if(err){

                                                    callback(err)

                                                }else{

                                                    callback(null,'Write new2.txt in filesnames.txt')
                                                    // 4. Read the new files, sort the content, write it out to a new file.
                                                    //Store the name of the new file in filenames.txt
                                                    fs.readFile(fileName,'utf-8',(err,data2) => {

                                                        if(err){
                                                            callback(err)
                                                        }else{
                                                            callback(null,'Read the new2.txt file')
                                                            fileName ='new3.txt'
                                                            fs.writeFile(fileName,JSON.stringify(JSON.parse(data2).sort()),(err) =>{

                                                                if(err){

                                                                    callback(err)

                                                                }else{

                                                                    callback(null,'Write the data in new3.txt file')
                                                                    fs.appendFile('filenames.txt',`${fileName}`,(err) => {

                                                                        if(err){
                        
                                                                            console(err)
                        
                                                                        }else{
                        
                                                                            callback(null,'Write new3.txt in filesnames.txt')
                                                                            // 5. Read the contents of filenames.txt and delete 
                                                                            //all the new files that are mentioned in that list simultaneously.
                                                                            fs.readFile('filenames.txt','utf-8',(err,data) => {
                                                                                if(err) {

                                                                                    callback(err)
                                                                                }else{
                                                                                    callback(null,'Read the data from filenames.txt')
                                                                                    const namesArr = data.split('\n')
                                                                                    namesArr.forEach( filename => {

                                                                                        fs.unlink(filename, (err) => {

                                                                                            if (err){
                                                                                                callback(err)
                                                                                            }else{
                                                                                                callback(null,`Delete ${filename} file`)
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                    
                                                                                }
                                                                            })

                                                                        }
                                                                    })

                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })


                }
            })


        }
    })
}

module.exports=problem2

